let app = new Vue({
	el: '#exchange',
	data() {
        return {
			      data: null,
            exg: "SEK",
		      	selectedFromCurrency: "SEK",
		      	selectedToCurrency: "USD",
			      inputAmount: 0,
            outputAmount: 0
    }
	},
	created(){
	},
	mounted() {
        //loads api
        this.url(this.exg)
  	},
  	methods: {
          //updates api with new currency
          clicked: function(value) {
              this.exg = value
              this.url(this.exg)
          },
          //when typing, this function will see which currency that have been chosen, and change the api if neccesary. then it will update the amount for input and output
          //key is for example "USD", "SEK" etc. 
          write: function(){
              let currency = {}
              var t = this

              axios
                .get('https://api.exchangeratesapi.io/latest?base=' + t.selectedFromCurrency)
                .then(function (response) {
              
                currency = response.data.rates;
                
                for (let key in currency) {
                  if (key == t.selectedToCurrency) {
                      t.outputAmount = (parseInt(t.inputAmount) * currency[key]);
                  }
                }
              })   
          },
          //simply switches the two currency variables, then call on the write function
          swap: function(selectedToCurrency, selectedFromCurrency) {
            let t = this
            t.selectedFromCurrency = selectedToCurrency
            t.selectedToCurrency = selectedFromCurrency
            t.write()
          },

          //the function for loading the api
          url: function(currencyURL) {
            axios
            .get('https://api.exchangeratesapi.io/latest?base=' + currencyURL)
            .then(response => (this.data = response.data))
        }
    }
})
      

